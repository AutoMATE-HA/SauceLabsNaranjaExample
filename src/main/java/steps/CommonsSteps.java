package steps;

//import com.incluit.automate.android.pages.CalcPage;
import com.incluit.automate.core.steps.StepBase;
import com.incluit.automate.saucelabs.osnative.pages.NaranjaMain;
import com.incluit.automate.saucelabs.osnative.pages.NaranjaPromo;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class CommonsSteps extends StepBase {


    @When("ingreso el dni $dni y el password $pass")
    public void login(String dni, String pass) throws Exception {
        getPage(NaranjaMain.class).logIn(dni,pass);
    }

    @When("hago click en el boton promocion")
    public void clickPromo() throws Exception {
        getPage(NaranjaMain.class).presionarPromociones();
    }

    @Then("la aplicacion se muestra")
    public void validateNaranjaMain() throws Exception {
        Assert.assertTrue(getPage(NaranjaMain.class).isDisplayed());
    }


    @When("elijo la provincia de $prov")
    public void elejirProvincia(String prov) throws Exception {
        getPage(NaranjaPromo.class).elegirProvicia(prov);
    }

    @When("elijo la localidad de $loc")
    public void elejirLocalidad(String loc) throws Exception {
        getPage(NaranjaPromo.class).elegirLocalidad(loc);
    }

    @When("elijo la promo numero $num")
    public void elejirLocalidad(int promo) throws Exception {
        getPage(NaranjaPromo.class).elegirPromo(promo);
    }

    @Then("la pagina de promocion se muestra")
    public void assertPromo() throws Exception {
        Assert.assertTrue(getPage(NaranjaPromo.class).isDisplayed());
    }

}
