package com.incluit.automate.saucelabs.osnative.pages.impl;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.incluit.automate.core.agent.Agent;
import com.incluit.automate.core.exceptions.AgentException;
import com.incluit.automate.core.page.BasePage;
import com.incluit.automate.core.structures.FlawedTimeUnit;
import com.incluit.automate.saucelabs.osnative.pages.NaranjaPromo;

public class NaranjaPromoImpl extends BasePage implements NaranjaPromo {
    By provincia = By.xpath("//android.widget.Spinner[@text='Todo el país']");
    By localidad = By.xpath("//android.widget.Spinner[@text='--- Localidad ---']");
    By promocionByIndex = By.xpath("//android.widget.Button[@text='\uF48A Mostrar más ']/parent::*");
    String option = "//android.widget.CheckedTextView[@text='%s']";
    FlawedTimeUnit wait = FlawedTimeUnit.seconds(6);
    /**
     * Constructor
     *
     * @param agent Agent
     */
    public NaranjaPromoImpl(Agent agent) {
        super(agent);
    }

    @Override
    public boolean isDisplayed() {
        return agent.checkElementIsDisplayed(provincia,FlawedTimeUnit.seconds(10));
    }

    @Override
    public void elegirProvicia(String provincia) throws AgentException {
        WebElement el = (WebElement) agent.findElement(this.provincia);
        elejirCombo(el,provincia);
    }

    @Override
    public void elegirLocalidad(String localidad) throws AgentException {
        WebElement el = (WebElement) agent.findElement(this.localidad);
        elejirCombo(el,localidad);
        wait.waitThisMuch();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void elegirPromo(int posicion) throws AgentException {
        List<WebElement> promociones = (List<WebElement>) agent.findElements(promocionByIndex);
        promociones.get(posicion-1).click();
    }

    private void elejirCombo(WebElement select, String option) throws AgentException {
        select.click();
        agent.click(By.xpath(String.format(this.option,option)));
    }
}
