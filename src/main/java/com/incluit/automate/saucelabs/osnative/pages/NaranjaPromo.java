package com.incluit.automate.saucelabs.osnative.pages;

import com.incluit.automate.core.exceptions.AgentException;

public interface NaranjaPromo {
    boolean isDisplayed();
    void elegirProvicia(String provincia) throws AgentException;
    void elegirLocalidad(String localidad) throws AgentException;
    void elegirPromo(int posicion) throws AgentException;
}
