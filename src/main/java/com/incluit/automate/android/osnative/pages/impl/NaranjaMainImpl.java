package com.incluit.automate.android.osnative.pages.impl;

import com.incluit.automate.android.osnative.pages.NaranjaMain;
import com.incluit.automate.core.agent.Agent;
import com.incluit.automate.core.exceptions.AgentException;
import com.incluit.automate.core.page.BasePage;
import com.incluit.automate.core.structures.FlawedTimeUnit;

import org.openqa.selenium.By;

public class NaranjaMainImpl extends BasePage implements NaranjaMain {
    By dni = By.xpath("//android.view.View[@index='0']/android.widget.EditText");
    By password = By.xpath("//android.view.View[@index='1']/android.widget.EditText");
    By logInButton = By.xpath("//android.widget.Button[contains(@text,'INGRESAR')]");
    By promociones = By.xpath("//android.widget.Button[contains(@text,'PROMOCIONES')]");
    By test = By.xpath("//android.view.View[@text='APP NARANJA']");
    /**
     * Constructor
     *
     * @param agent Agent
     */
    public NaranjaMainImpl(Agent agent) {
        super(agent);
    }

    @Override
    public boolean isDisplayed() {
        return agent.checkElementIsDisplayed(test,FlawedTimeUnit.seconds(3));
    }

    @Override
    public void presionarPromociones() throws AgentException {
        agent.click(promociones);
    }

    @Override
    public void logIn(String user, String password) throws AgentException {
        agent.writeInElement(this.dni, user);
        agent.writeInElement(this.password, password);
        agent.click(logInButton);
    }
}
