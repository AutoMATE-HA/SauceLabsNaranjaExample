package com.incluit.automate.android.osnative.pages;

import com.incluit.automate.core.exceptions.AgentException;

public interface NaranjaMain {
    boolean isDisplayed();
    void presionarPromociones() throws AgentException;
    void logIn(String user, String password) throws AgentException;
}
