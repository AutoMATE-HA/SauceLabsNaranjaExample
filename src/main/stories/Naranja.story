Validate that the user can edit the profile name

Narrative:
In order to validate that the user can change his name
As a user
I want to go to the profile edit page and change the user's name
					 
Scenario: Edit name
Given crear una instancia de SauceLabs con el dispositivo <device> y version de android <version>
Then la aplicacion se muestra
When ingreso el dni 27657349 y el password asd
When hago click en el boton promocion
Then la pagina de promocion se muestra
When elijo la provincia de Córdoba
And elijo la localidad de Ballesteros
And elijo la promo numero 1

Examples:
|device|version|
|Motorola_Moto_E2_real_us|5.1|
|Samsung_Galaxy_S6_sjc_free|7.1|